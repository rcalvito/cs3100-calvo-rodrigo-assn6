#include "MyVector.hpp"
#include <mutex>
#include <unordered_map>

void MyVector::add(int key, int value){
	std::lock_guard<std::mutex> lock(digitsMutex);
	digits.insert({key,value});
	return;

}

int MyVector::take(){
	std::lock_guard<std::mutex> lock(dataMutex);
	int temp = data.back();
	data.pop_back();
	return temp;	


}

void MyVector::fill(){
	for(int i=1;i<=1000;i++){
		data.push_back(i);
	}
}

void MyVector::print(){
	std::cout<<"3.";
	for(int i=1;i<=1000;i++){
		std::unordered_map<int,int>::const_iterator element = digits.find(i);
		std::cout<<element->second;
	
	}
}

bool MyVector::isEmpty(){
	return data.empty();
}
