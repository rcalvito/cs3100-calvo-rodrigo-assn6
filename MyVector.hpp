#pragma once

#include <iostream>
#include <vector>
#include <mutex>
#include <unordered_map>



class MyVector {
	public:
		MyVector(){
			data = std::vector<int>(0);
			digits = std::unordered_map<int,int>({});
		}
		MyVector(const MyVector& rhs){
			data = rhs.data;
			digits = rhs.digits;
		}
		int take();
		void print();
		void add(int key, int value);
		void fill();
		bool isEmpty();



	private:
		std::vector<int> data;
		std::mutex dataMutex;
		std::mutex digitsMutex;
		std::unordered_map<int,int> digits;


};


