#include <iostream>
#include <vector>
#include <mutex>
#include <unordered_map>
#pragma once


class myVector {
	public:
		myVector(){
			data = std::vector<int>();
			digits = std::unordered_map<int,int>();
		}

		int take();
		void print();
		void add(int key, int value);




	private:
		std::vector<int> data;
		std::mutex dataMutex;
		std::mutex digitsMutex;
		std::unordered_map<int,int> digits;


};


