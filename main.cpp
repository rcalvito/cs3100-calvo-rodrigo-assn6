#include <iostream>
#include <thread>
#include <vector>
#include <memory>
#include <chrono>
#include "computePi.hpp"
#include "MyVector.hpp"
#include <mutex>

void threadWorker(MyVector* data) {
	while(!data->isEmpty()){
		int temp = data->take();
		data->add(temp, computePiDigit(temp));
		std::cout<<".";	
		std::cout.flush();
	}
}


int main() {
	auto before = std::chrono::high_resolution_clock::now();	
	//
	// Make as many threads as there are CPU cores
    // Assign them to run our threadWorker function, and supply arguments as necessary for that function


	MyVector* data =new MyVector();
	data->fill();	
	std::vector<std::shared_ptr<std::thread>> threads;
	for (std::uint16_t core = 0; core < std::thread::hardware_concurrency(); core++)
        // The arguments you wish to pass to threadWorker are passed as
        // arguments to the constructor of std::thread
		threads.push_back(std::make_shared<std::thread>(threadWorker,data));

	//
	// Wait for all of these threads to complete
	for (auto&& thread : threads)
		thread->join();

	std::cout << std::endl << std::endl;

		
	

	data->print();
	std::cout<<std::endl<<std::endl;
	auto after = std::chrono::high_resolution_clock::now();	
	std::chrono::duration<double> dur = after-before;
	std::cout<<"It took: "<<dur.count()<<" seconds from start to finish"<<std::endl;	
	return 0;
}
